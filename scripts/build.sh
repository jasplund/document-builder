#!/bin/bash

echo Starting build...
echo --------------------
echo Deleting old build

rm -rf output
rm input/zzz.md

echo --------------------------------
echo old build deleted...
echo --------------------------------
echo Creating new folder structure...

mkdir output
mkdir output/html
mkdir output/docx
mkdir output/pdf

echo --------------------------------
echo folder structure created...
echo --------------------------------
echo Creating version control data

TAG=$(git describe --long --tags --always)

echo  > input/zzz.md
echo "## Version control  " >> input/zzz.md
echo  >> input/zzz.md
echo "Document version: $TAG" >> input/zzz.md

echo  >> input/zzz.md
echo "## Change log  " >> input/zzz.md
echo  >> input/zzz.md
echo 5 last commits are shown here.  >> input/zzz.md
echo  >> input/zzz.md
git log --max-count=5 --pretty=format:"%h%x09%an%x09%ad%x09%s%n" --date=short >> input/zzz.md


echo -------------------------------
echo Version control data created
echo -------------------------------
echo Cooking new build...

pandoc input/* -s -o output/html/webpage.html --toc
pandoc input/* -s -o output/docx/document.docx --toc \
#
# uncomment this line if reference docx file should be used
#
#--reference-docx=references/reference.docx

pandoc input/* -s -o output/pdf/document.pdf --toc

echo -------------------------------
echo build finished...
