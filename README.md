# JAapp Document Builder

This is a project to test if its possible to create and maintain a text under version control using GIT and to create various types of output documents using pandoc as a converter.

## Before you start

You need to install the following programs prior to working with this package

1. [GIT](https://git-scm.com/)
2. [Pandoc](http://pandoc.org/)
3. [Miktex](http://miktex.org/)

It might also be good to install a good markdown friendly markdown text editor. I found a nice one here: [Texts](http://www.texts.io/).

## Workflow  

I assume you already know how GIT works.

1. Create as many markdown files as you want in the input folder (don't create any new folders inside input)
2. You could name the files as you want. The files will be parsed in alphabetical order
3. cd into your folder `cd /your/project/folder/`
4. make `./scripts/build.sh` executable (in OSX: `chmod +x ./scripts/build.sh`)
5. run `./scripts/build.sh`
6. BOOM, you now got yourself a output folder containing a pdf, a Word document (docx) and a html-webpage
